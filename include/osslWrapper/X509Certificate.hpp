#pragma once

#include <osslWrapper/pointer_types.hpp>

#include <cstdint>
#include <memory>

#include <openssl/x509.h>

namespace osslwrapper {

typedef struct {
    const uint8_t* m_data;
    size_t m_size;
} ByteSpan;

class X509Certificate {
public:
    static X509Certificate fromPem(ByteSpan bytes);

    X509Certificate() = default;

    X509Certificate(const X509Certificate&) = delete;
    X509Certificate& operator=(const X509Certificate&) = delete;

    X509Certificate(X509Certificate&&) = default;
    X509Certificate& operator=(X509Certificate&&) = default;

    [[nodiscard]] size_t getVersion() const;
    void setVersion(size_t version);

private:
    explicit X509Certificate(uniqueX509 x509);

    uniqueX509 m_x509;
};

} // osslwrapper
