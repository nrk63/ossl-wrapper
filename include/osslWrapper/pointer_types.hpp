#pragma once

#include <memory>
#include <type_traits>

#include <openssl/x509.h>

namespace osslwrapper {

using uniqueX509 =
    std::unique_ptr<X509, std::integral_constant<decltype(X509_free)*, &X509_free>>;

} // osslwrapper

