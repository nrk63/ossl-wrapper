#include <osslWrapper/X509Certificate.hpp>

#include <stdexcept>

#include <openssl/pem.h>

namespace {
    constexpr int kOpensslSuccess = 1;
} // namespace

namespace osslwrapper {

X509Certificate X509Certificate::fromPem(osslwrapper::ByteSpan bytes) {
    BIO* bioWithCert = BIO_new(BIO_s_mem());
    BIO_write(bioWithCert, bytes.m_data, bytes.m_size);

    X509* osslCert = PEM_read_bio_X509(bioWithCert, nullptr, nullptr, nullptr);
    if (!osslCert) {
        throw std::runtime_error { "unable to parse certificate" };
    }
    BIO_free(bioWithCert);

    uniqueX509 x509 { osslCert };
    return X509Certificate { std::move(x509) };
}

size_t X509Certificate::getVersion() const {
    return X509_get_version(m_x509.get()) + 1;
}

void X509Certificate::setVersion(size_t version) {
    if (version == 0) {
        throw std::invalid_argument { "version must be positive" };
    }

    if (X509_set_version(m_x509.get(), version - 1) != kOpensslSuccess) {
        throw std::runtime_error { "unable to set version" };
    }
}

X509Certificate::X509Certificate(uniqueX509 x509)
    : m_x509(std::move(x509)) {}

} // osslwrapper
