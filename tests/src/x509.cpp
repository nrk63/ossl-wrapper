#include <gtest/gtest.h>

#include <cstdint>
#include <vector>
#include <filesystem>
#include <string_view>
#include <string>
#include <fstream>

#include <osslWrapper/X509Certificate.hpp>

namespace {

std::vector<uint8_t> readFile(std::string_view filename) {
    std::filesystem::path filePath { filename };
    std::filesystem::directory_entry fileEntry { filePath };

    if (!fileEntry.exists())
        throw std::invalid_argument { "no such file: " + std::string { filename } };

    if (!fileEntry.is_regular_file())
        throw std::invalid_argument { "file is not regular: " + std::string { filename } };

    auto fileSize = fileEntry.file_size();

    std::ifstream in { filePath };
    std::vector<uint8_t> result(fileSize, 0);
    in.read(reinterpret_cast<char*>(result.data()), result.size());

    return result;
}

} // namespace

using namespace osslwrapper;

TEST(X509, Version) {
    auto testData = readFile("data/basic-secp521r1.pem");
    ByteSpan span { testData.data(), testData.size() };
    auto x509 = X509Certificate::fromPem(span);

    EXPECT_EQ(x509.getVersion(), 1);

    x509.setVersion(3);
    EXPECT_EQ(x509.getVersion(), 3);
}
