# Fetching GoogleTest library
enable_testing()
include(FetchContent)
FetchContent_Declare(
    googletest
    GIT_REPOSITORY https://github.com/google/googletest.git
    GIT_TAG release-1.12.1
)
# For Windows: Prevent overriding the parent project's compiler/linker settings
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)

add_executable(osslWrapperTests "")

target_include_directories(osslWrapperTests PRIVATE ${OPENSSL_INCLUDE_DIR})

target_link_libraries(
    osslWrapperTests
        GTest::gtest_main
        osslWrapper
)

file(COPY data DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
add_subdirectory(src)
